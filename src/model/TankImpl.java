package model;

public class TankImpl implements Tank {
	private int lifes;
	private double x;
	private double y;
	private static double SPEED = 3;
	private double speedX = 0;
	private double speedY = 0;
	private Cannon cannon = new Cannon();
	
	public TankImpl(double x, double y, int lifes) {
		this.x = x;
		this.y = y;
		this.lifes = lifes;
	}

	@Override
	public boolean isAlive() {
		return this.lifes!=0;
	}
	
	@Override
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public double getX() {
		return this.x;
	}

	@Override
	public double getY() {
		return this.y;
	}

	@Override
	public void shot() {
		
	}
	
	public void updateCannon(double x, double y) {
		
	}

	@Override
	public void setSpeed(Direction dir, boolean b) {
		if (dir == Direction.UP && b == true) {
			this.speedY = -SPEED;
		} else if (dir == Direction.UP && b == false) {
			this.speedY = 0;
		} else if (dir == Direction.DOWN && b == true) {
			this.speedY = SPEED;
		} else if (dir == Direction.DOWN && b == false) {
			this.speedY = 0;
		} else if (dir == Direction.RIGHT && b == true) {
			this.speedX = SPEED;
		} else if (dir == Direction.RIGHT && b == false) {
			this.speedX = 0;
		} else if (dir == Direction.LEFT && b == true) {
			this.speedX = -SPEED;
		} else if (dir == Direction.LEFT && b == false) {
			this.speedX = 0;
		}
	}

	@Override
	public void updatePosition() {
		 this.x += this.speedX;
		 this.y += this.speedY;
	}
	
	private class Cannon {
		
		private double angle;
		
		public void setAngle(double angle) {
			this.angle = angle;
		}
		
		public void shot() {
			
		}
	}
	
}
