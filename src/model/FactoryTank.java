package model;

public class FactoryTank {
	private static double DEFAULT_PLAYER_X;
	private static double DEFAULT_PLAYER_Y;
	private static double DEFAULT_ENEMY_X;
	private static double DEFAULT_ENEMY_Y;
	
	public Tank createPlayerTankWithLife(final int lifes) {
		return new TankImpl(DEFAULT_PLAYER_X, DEFAULT_PLAYER_Y, lifes);
	}
	
	public Tank createEnemyTankWithLife(final int lifes) {
		return new TankImpl(DEFAULT_ENEMY_X, DEFAULT_ENEMY_Y, lifes);
	}
}
