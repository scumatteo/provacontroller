package model;

public interface Tank {
	public boolean isAlive();
	public void setPosition(double x, double y);
	public double getX();
	public double getY();
	public void shot();
	public void setSpeed(Direction dir, boolean b);
	public void updatePosition();
	
}
