package controller;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Tank;

public class GameLoopImpl extends Application implements GameLoop {
	
	private Stage stage;
	private Scene scene;
	private ControllerTank playerTank;
	private ControllerTank enemyTank;
	
	public GameLoopImpl(Stage stage, Scene scene) {
		this.stage = stage;
		this.scene = scene;
	}
	
	/*public GameLoopImpl(ControllerTank myTank, ControllerTank enemyTank) {
		this.myTank = myTank;
		this.enemyTank = enemyTank;
	}*/ //forse meglio costruttore che setter

	@Override
	public void start(Stage currentStage) throws Exception {
		new AnimationTimer() {
			
			@Override
			public void handle(long arg0) {
				updatePlayerTank();
				updateEnemyTank();
				view.draw(playerTank);
				view.draw(enemyTank);
				
			}
		}.start();
		
	}

	@Override
	public void updatePlayerTank() {
		playerTank.movePlayerTank(scene);
		
	}

	@Override
	public void updateEnemyTank() {
		enemyTank.IA();
		
	}

	@Override
	public void setPlayerTank(ControllerTank playerTank) {
		this.playerTank = playerTank;
		
	}

	@Override
	public void setEnemyTank(ControllerTank enemyTank) {
		this.enemyTank = enemyTank;
		
	}

}
