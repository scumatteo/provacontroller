package controller;

public interface GameLoop {
	void updatePlayerTank();
	
	void updateEnemyTank();
	
	void setPlayerTank(ControllerTank playerTank);
	
	void setEnemyTank(ControllerTank enemyTank);
}
