package controller;

import javafx.scene.Scene;
import model.Direction;

public interface ControllerTank {
	
	void movePlayerTank(Scene s);
	
	void moveEnemyTank(Direction dir, boolean b);
	
	void IA();
	
	void keepBeetwenBorder(double sceneWidht, double sceneHeight, double xImage, double yImage);

}
