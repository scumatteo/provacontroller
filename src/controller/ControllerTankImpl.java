package controller;

import javafx.scene.Scene;
import model.FactoryTank;
import model.Tank;
import model.Direction;

public class ControllerTankImpl implements ControllerTank {
	
	private FactoryTank tank = new FactoryTank();
	private Tank playerTank = tank.createPlayerTankWithLife(3);
	private Tank enemyTank = tank.createEnemyTankWithLife(3);
	private double sceneWidht;
	private double sceneHeight;
	private double  imageWidht;
	private double imageHeight;
	
	public ControllerTankImpl(double sceneWidht, double sceneHeight, double imageWidht, double imageHeight) {
		this.sceneWidht = sceneWidht;
		this.sceneHeight = sceneHeight;
		this.imageWidht = imageWidht;
		this.imageHeight = imageHeight;
	}
	
	@Override
	public void movePlayerTank(Scene s) {
		s.setOnKeyPressed(e ->{
			 switch (e.getCode()) {
	            case UP: playerTank.setSpeed(Direction.UP, true); break;
	            case DOWN: playerTank.setSpeed(Direction.DOWN, true); break;
	            case LEFT: playerTank.setSpeed(Direction.LEFT, true); break;
	            case RIGHT: playerTank.setSpeed(Direction.RIGHT, true); break;
	            default: ;
	            }
		});
		
		s.setOnKeyReleased(e -> {
			switch(e.getCode()) {
			 	case UP: playerTank.setSpeed(Direction.UP, false); break;
	            case DOWN: playerTank.setSpeed(Direction.DOWN, false); break;
	            case LEFT: playerTank.setSpeed(Direction.LEFT, false); break;
	            case RIGHT: playerTank.setSpeed(Direction.RIGHT, false); break;
            default: ;
			}			
		});
		
		playerTank.updatePosition();
		this.keepBeetwenBorder(this.sceneWidht, this.sceneHeight, this.imageWidht, this.imageHeight);
		playerTank.updatePosition();
		
	}
		
		@Override
	public void keepBeetwenBorder(double sceneWidht, double sceneHeight, double imageWidht, double imageHeight) {
		if (playerTank.getX() + imageWidht >= sceneWidht) {       // Exceeding right
			playerTank.setPosition(sceneWidht - imageWidht, playerTank.getY());
		} else if (playerTank.getX() < 0) {                // Exceeding left
			playerTank.setPosition(0, playerTank.getY());
		}
		if (playerTank.getY() + imageHeight >= sceneHeight) {      // Exceeding down
			playerTank.setPosition(playerTank.getX(), sceneHeight - imageHeight); 
		} else if (playerTank.getY() < 0) {                // Exceeding up
			playerTank.setPosition(playerTank.getX(), 0);
		}
	}
		
	

	@Override
	public void moveEnemyTank(Direction dir, boolean b) {
		enemyTank.setSpeed(dir, b);
		
	}

	@Override
	public void IA() {
		if(playerTank.getX() > enemyTank.getX()) {
			this.moveEnemyTank(Direction.RIGHT, true);
			this.moveEnemyTank(Direction.LEFT, false);
		}
		else if(playerTank.getX() < enemyTank.getX()){
			this.moveEnemyTank(Direction.LEFT, true);
			this.moveEnemyTank(Direction.RIGHT, false);
		}
		else if(playerTank.getX() == enemyTank.getX()){
			this.moveEnemyTank(Direction.RIGHT, false);
			this.moveEnemyTank(Direction.LEFT, false);
		}
		if(playerTank.getY() > enemyTank.getY()) {
			this.moveEnemyTank(Direction.DOWN, true);
			this.moveEnemyTank(Direction.UP, false);
		}
		else if(playerTank.getY() < enemyTank.getY()){
			this.moveEnemyTank(Direction.UP, true);
			this.moveEnemyTank(Direction.DOWN, false);
		}
		else if(playerTank.getY() == enemyTank.getY()){
			this.moveEnemyTank(Direction.UP, false);
			this.moveEnemyTank(Direction.DOWN, false);
		}
		
	}
	
	
	


	

}
